//
//  AppDelegate.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import UIKit
import PluggableAppDelegate

@UIApplicationMain
class AppDelegate: PluggableApplicationDelegate {
    
    static let shared = UIApplication.shared.delegate as! AppDelegate
    
    let initService = InitService()

    override init() {
        super.init()
        self.window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    override var services: [ApplicationService] {
        return [initService
           ]
    }
}
