//
//  InitService.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import PluggableAppDelegate

final class InitService: NSObject, ApplicationService {

    var appRootView: UIViewController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        initApp()
        return true
    }
    
    private func initApp() {
        guard let window = window else {return}
        let vc =  MainViewController()
        vc.configurator = MainConfiguratorImplementation()
        appRootView = vc
        
        window.rootViewController = appRootView
        window.makeKeyAndVisible()
        window.layer.cornerRadius = 4
        window.clipsToBounds = true
    }
}
