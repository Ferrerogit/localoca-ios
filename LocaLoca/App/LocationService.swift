//
//  LocationService.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import PluggableAppDelegate

final class LocationService: NSObject, ApplicationService {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        initLocationService()
        return true
    }
    
    private func initLocationService() {
        
    }
    
}
