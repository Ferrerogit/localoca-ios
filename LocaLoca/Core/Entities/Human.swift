//
//  Location.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit.UIImage
import CoreLocation.CLLocation

class Human: Hashable {
    static func == (lhs: Human, rhs: Human) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id: String!
    var name: String!
    var avatar: UIImage!
    var location: CLLocation!
    
    func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
}
