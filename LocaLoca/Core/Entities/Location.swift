//
//  Location.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

struct Location: Equatable {
    var latitude: Double!
    var longitude: Double!
}

func == (lhs: Location, rhs: Location) -> Bool {
    return lhs.latitude.precised(10) == rhs.latitude.precised(10)
        && lhs.longitude.precised(10) == rhs.longitude.precised(10)
}
