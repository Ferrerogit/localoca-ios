//
//  Route.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 03/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import CoreLocation

struct Route: Hashable {
    static func == (lhs: Route, rhs: Route) -> Bool {
        return lhs.begin == rhs.begin && lhs.end == rhs.end
    }
    
    var begin: Human?
    var distance: CLLocationDistance?
    var end: Human!
    
    var hashValue: Int {
        return (begin?.id ?? "").hashValue + end.id.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(begin?.id)
        hasher.combine(end.id)
    }
}
