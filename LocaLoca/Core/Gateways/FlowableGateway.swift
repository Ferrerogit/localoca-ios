//
//  FlowableGateway.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol FlowableGateway {
    associatedtype T
    var eventHandler: ((_ people: Result<T>) -> Void)? { get set }
}
