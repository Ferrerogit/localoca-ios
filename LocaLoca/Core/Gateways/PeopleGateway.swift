//
//  PeopleGateway.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

typealias FetchPeopleEntityGatewayCompletionHandler = (_ people: Result<[Human]>) -> Void

protocol PeopleGateway {
    func fetchPeople(completionHandler: @escaping FetchPeopleEntityGatewayCompletionHandler)
}
