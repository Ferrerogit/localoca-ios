//
//  MainUseCase.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import CoreLocation

typealias MainUseCaseCompletionHandler = (_ people: Result<[Route]>) -> Void

protocol MainUseCase {
    func displayDistances(begin: Human?, completionHandler: @escaping MainUseCaseCompletionHandler)
}

class MainUseCaseImplementation: MainUseCase {
    func displayDistances(begin: Human?, completionHandler: @escaping MainUseCaseCompletionHandler) {
        self.peopleGateway.fetchPeople { result in
            switch result {
            case .success(let people):
                var routes = [Route]()
                for human in people {
                    if human == begin {continue}
                    var route = Route()
                    route.begin = begin
                    route.end = human
                    
                    route.distance = begin?.location.distance(from: route.end.location)
                    routes.append(route)
                }
                
                DispatchQueue.main.async {
                      completionHandler(.success(routes))
                }
            case .failure:
                break
            }
        }
    }
    
    let peopleGateway: PeopleGateway
    
    init(peopleGateway: PeopleGateway) {
        self.peopleGateway = peopleGateway
    }
}
