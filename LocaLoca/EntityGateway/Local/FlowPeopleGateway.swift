//
//  FlowPeopleGateway.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit.UIImage
import CoreLocation.CLLocation

class FlowPeopleGateway: PeopleGateway, FlowableGateway {
    var eventHandler: ((Result<[Human]>) -> Void)?
    typealias T = [Human]
    
    let t = RepeatingTimer(timeInterval: 1)
    
    private var people: [Human] = []
    private var beginner: Human? = nil
    private let initalSource : [String] = [
        "Darya Telisheva",
        "Kirill Moshkov",
        "Nikita Bekrenev",
        "Yaroslav Polichev",
        "Yury Skrelin"
    ]
    
    
    init(count: Int) {
        for _ in 0..<count {
            let human = Human()
            // Russia's geographic range
            let location = CLLocation(latitude: Double.random(in: 49...73), longitude: Double.random(in: 32...138))
            human.location = location
            
            let randomInitial = initalSource.randomElement()!
            
            human.id = UUID().uuidString
            human.name = randomInitial
            human.avatar = UIImage(named: randomInitial)!
            
            people.append(human)
        }
    }
    
    func fetchPeople(completionHandler: @escaping FetchPeopleEntityGatewayCompletionHandler) {
        t.suspend()
        t.eventHandler = {
            self.updateUsers(completionHandler: completionHandler)
        }
        t.resume()
    }
    
    private func updateUsers(completionHandler: @escaping FetchPeopleEntityGatewayCompletionHandler) {
    
        for human in people {
            let newLongitude = human.location.coordinate.longitude + Double.random(in: -0.0005...0.0005)
            let newLatitude = human.location.coordinate.latitude + Double.random(in: -0.0005...0.0005)
            human.location = CLLocation(latitude: newLatitude, longitude: newLongitude)
        }
        
        completionHandler(.success(people))
    }
}
