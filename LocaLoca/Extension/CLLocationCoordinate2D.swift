//
//  CLLocation.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 04/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import MapKit
import Foundation

extension CLLocationCoordinate2D {
    
    func describing() -> String? {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 5
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 5
        
        if let latString = formatter.string(from: NSNumber(value: latitude)),
            let longString = formatter.string(from: NSNumber(value: longitude)) {
            return "Lat: \(latString), Long: \(longString)"
        }
        
        return nil
    }
}
