//
//  UIColor.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var backgroundBlue: UIColor {
        return UIColor(red: 19 / 255, green: 25 / 255, blue: 34 / 255, alpha: 1)
    }
    
    static var darkSeparator: UIColor {
        return UIColor(red: 26 / 255, green: 36 / 255, blue: 49 / 255, alpha: 1)
    }
    
    static var darkText: UIColor {
        return UIColor(red: 251 / 255, green: 251 / 255, blue: 251 / 255, alpha: 1)
    }
    
    static var findMyFriendsGreen: UIColor {
        return UIColor(red:67 / 255, green: 213 / 255, blue: 81 / 255, alpha: 1)
    }
}
