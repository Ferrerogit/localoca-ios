//
//  UITableViewCell.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    /// Default reuse identifier should be the same as object class name
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
    
    private class func loadInstance<CellType: UITableViewCell>(_ tableView: UITableView, _ indexPath: IndexPath?, identifier: String? = nil) -> CellType? {
        if let indexPath = indexPath {
            return tableView.dequeueReusableCell(withIdentifier: identifier ?? defaultReuseIdentifier, for: indexPath) as? CellType
        } else {
            return tableView.dequeueReusableCell(withIdentifier: identifier ?? defaultReuseIdentifier) as? CellType
        }
    }
    
    class func instance(_ tableView: UITableView, _ indexPath: IndexPath? = nil, identifier: String? = nil) -> Self? {
        return loadInstance(tableView, indexPath, identifier: identifier)
    }
}
