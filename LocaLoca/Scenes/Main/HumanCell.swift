//
//  HumanCell.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class HumanCell: UITableViewCell {
    
    let name = UILabel()
    let avatar = UIImageView()
    let distanceLabel = UILabel()
    let locationLabel = UILabel()
    
    private var cachedWidth: CGFloat? = nil
    
    private let numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 1
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.minimumFractionDigits = 1
        numberFormatter.decimalSeparator = "."
        return numberFormatter
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        contentView.addSubview(avatar)
        avatar.easy.layout(
            Size(48),
            Left(16),
            CenterY()
        )
        avatar.contentMode = .scaleAspectFill
        avatar.backgroundColor = .gray
        avatar.clipsToBounds = true
        avatar.layer.cornerRadius = 48 / 2
        
        contentView.addSubview(distanceLabel)
        distanceLabel.easy.layout(
            Right(16),
            CenterY()
        )
        
        distanceLabel.layer.cornerRadius = 3
        distanceLabel.clipsToBounds = true
        distanceLabel.textColor = .white
        distanceLabel.backgroundColor = .findMyFriendsGreen
        distanceLabel.font = .systemFont(ofSize: 15)
        distanceLabel.adjustsFontSizeToFitWidth = true
        distanceLabel.textAlignment = .center
        
        contentView.addSubview(name)
        name.easy.layout(
            Left(8).to(avatar),
            CenterY(-8).to(avatar)
        )
        
        contentView.addSubview(locationLabel)
        
        locationLabel.easy.layout(
            Left(8).to(avatar),
            CenterY(8).to(avatar)
        )
        
        locationLabel.font = .systemFont(ofSize: 10)
        locationLabel.textColor = .gray
    }
    
    func configure(_ route: Route) {
        let human = route.end!
        name.text = human.name
        avatar.image = human.avatar
        UIView.transition(with: locationLabel,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.locationLabel.text = human.location.coordinate.describing()
        })
        
        guard let distance = route.distance else {return}
       
        let kilometers = numberFormatter.string(from: NSNumber(value: distance / 1000.0))
        
        UIView.transition(with: distanceLabel, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.distanceLabel.text = "\(kilometers!) Km"
           })
        
        
        if cachedWidth == nil {
            distanceLabel.sizeToFit()
            cachedWidth = distanceLabel.frame.width + 24
        }
        
        distanceLabel.easy.layout(
            Width(cachedWidth!)
        )

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cachedWidth = nil
    }
}
