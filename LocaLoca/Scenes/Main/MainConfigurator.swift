//
//  MainConfigurator.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol MainConfigurator {
    func configure(mainViewController: MainViewController)
}

class MainConfiguratorImplementation: MainConfigurator {
    
    func configure(mainViewController: MainViewController) {
        
        let peopleGateway = FlowPeopleGateway(count: 30)
        
        let mainUseCase = MainUseCaseImplementation(peopleGateway: peopleGateway)
        let router = MainViewRouterImplementation(mainViewController: mainViewController)
        
        let presenter = MainPresenterImplementation(view: mainViewController, mainUseCase: mainUseCase, router: router)
        
        mainViewController.presenter = presenter
    }
}
