//
//  MainPresenter.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol MainView: class {
    var routes: [Route] {get set}
}

protocol MainPresenter {
    var router: MainViewRouter { get }
    
    func viewDidLoad()
    func didPressed(human: Human)
}

class MainPresenterImplementation: MainPresenter {
    func viewDidLoad() {
        displayDistances()
    }
    
    private func displayDistances() {
        self.mainUseCase.displayDistances(begin: begin) { result in
            switch result {
            case .success(let value):
                self.view?.routes = value
            case .failure(_):
                break
            }
        }
    }
    
    func didPressed(human: Human) {
        self.begin = human
        displayDistances()
    }
    
    fileprivate weak var view: MainView?
    fileprivate var mainUseCase: MainUseCase
    private(set) var router: MainViewRouter
    
    private var begin: Human? = nil
    
    init(view: MainView,
         mainUseCase: MainUseCase,
         router: MainViewRouter) {
        self.view = view
        self.mainUseCase = mainUseCase
        self.router = router
    }
    
}
