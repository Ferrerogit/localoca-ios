//
//  MainRouter.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol MainViewRouter: ViewRouter {
    func dismiss()
}

class MainViewRouterImplementation: MainViewRouter {
    fileprivate weak var mainViewController: MainViewController?
    
    init(mainViewController: MainViewController) {
        self.mainViewController = mainViewController
    }
    
    func dismiss() {
        mainViewController?.dismiss(animated: true, completion: nil)
    }
}
