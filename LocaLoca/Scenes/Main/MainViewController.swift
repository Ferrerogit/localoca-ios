//
//  MainViewController.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit
import GSKStretchyHeaderView
import EasyPeasy

class MainViewController: UITableViewController {
    var presenter: MainPresenter!
    var configurator: MainConfigurator!

    var beginHumanView: BeginHumanView!
    
    var routes = [Route]() {
        didSet {
            beginHumanView.configure(human: routes.first?.begin)
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(mainViewController: self)
        presenter.viewDidLoad()
        setup()
    }
    
    private func setup() {
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let headerSize = CGSize(width: UIScreen.main.bounds.width,
                                height: 200)
        beginHumanView =  BeginHumanView(frame: CGRect(x: 0, y: 0,
                                                   width: headerSize.width,
                                                   height: headerSize.height))
        beginHumanView.expansionMode = .topOnly
        
        beginHumanView.minimumContentHeight = 0
        beginHumanView.maximumContentHeight = 300
        beginHumanView.contentShrinks = false
        beginHumanView.contentExpands = true
    
        beginHumanView.contentAnchor = .bottom

        tableView.showsVerticalScrollIndicator = false
        tableView.addSubview(self.beginHumanView)
        view.backgroundColor = .white
        tableView.register(HumanCell.self, forCellReuseIdentifier: HumanCell.defaultReuseIdentifier)
        tableView.rowHeight = 80
        tableView.estimatedRowHeight = 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = HumanCell.instance(tableView) else {fatalError()}
        cell.configure(routes[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.didPressed(human: routes[indexPath.row].end)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
       tableView.bounces = tableView.contentOffset.y < tableView.contentSize.height - tableView.frame.height
    }
}

extension MainViewController: MainView {
    
}
