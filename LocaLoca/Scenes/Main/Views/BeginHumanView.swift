//
//  BeginHumanView.swift
//  LocaLoca
//
//  Created by Ярослав Попов on 02/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import MapKit
import EasyPeasy
import GSKStretchyHeaderView

class BeginHumanView: GSKStretchyHeaderView, MKMapViewDelegate {
    
    private var currentHuman: Human? = nil
    
    let backgroundMap = MKMapView()
    let singleAnnotation = HumanAnnotation()
    let annotationView = MKAnnotationView()
    
    let infoContainer = UIView()
    let avatar = UIImageView()
    let nameLabel = UILabel()
    let coordinatesLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        clipsToBounds = true
        addSubview(backgroundMap)
        backgroundMap.easy.layout(
            Edges()
        )
        
        backgroundMap.delegate = self
        backgroundMap.isUserInteractionEnabled = false
        backgroundMap.contentMode = .scaleAspectFill
        backgroundMap.mapType = .hybrid
        
        addSubview(infoContainer)
        infoContainer.easy.layout(
            Height(75),
            Width(UIScreen.main.bounds.width * 0.8),
            Left(8),
            Bottom(8)
        )
        infoContainer.layer.cornerRadius = 75 / 2
        infoContainer.isHidden = true
        infoContainer.clipsToBounds = true
        infoContainer.backgroundColor = .backgroundBlue
        infoContainer.backgroundColor = infoContainer.backgroundColor?.withAlphaComponent(0.5)
        
        infoContainer.addSubview(avatar)
        avatar.easy.layout(
            Left(6),
            Size(48),
            CenterY()
        )
        avatar.layer.cornerRadius = 48 / 2
        avatar.contentMode = .scaleAspectFill
        avatar.backgroundColor = .gray
        avatar.clipsToBounds = true
        
        infoContainer.addSubview(nameLabel)
        nameLabel.easy.layout(
            Left(8).to(avatar),
            CenterY(-8).to(avatar),
            Right(8)
        )
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.textColor = .darkText
        nameLabel.font = .systemFont(ofSize: 17)
        
        infoContainer.addSubview(coordinatesLabel)
        coordinatesLabel.easy.layout(
            Left(8).to(avatar),
            CenterY(8).to(avatar),
            Right(8)
        )
        coordinatesLabel.adjustsFontSizeToFitWidth = true
        coordinatesLabel.textColor = .darkText
        coordinatesLabel.font = .systemFont(ofSize: 13)
    }
    
    private func updateCoordinates(coordinates: CLLocationCoordinate2D) {
        coordinatesLabel.text = coordinates.describing()
        setAvatarToMap()
        backgroundMap.addAnnotation(singleAnnotation)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.25)
        singleAnnotation.coordinate = coordinates
        UIView.commitAnimations()
        
        backgroundMap.showAnnotations([singleAnnotation], animated: true)
    }
    
    
    func configure(human: Human?) {
        guard let human = human else {
            self.infoContainer.isHidden = true
            return
        }
        if human != self.currentHuman {
            self.currentHuman = human
            changeHuman(to: human)
        }
        
        updateCoordinates(coordinates: human.location.coordinate)
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return annotationView
    }
    
    private func setAvatarToMap() {
        annotationView.easy.layout(
            Size(48)
        )
        
        annotationView.clipsToBounds = true
        annotationView.layer.cornerRadius = 24
        annotationView.layer.borderWidth = 2
        annotationView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        annotationView.image = currentHuman?.avatar
    }
    
    private func changeHuman(to human: Human) {
        backgroundMap.removeAnnotation(singleAnnotation)
        backgroundMap.addAnnotation(singleAnnotation)
        infoContainer.isHidden = false
        nameLabel.text = human.name
        UIView.transition(with: avatar,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.avatar.image = human.avatar
        })
        
        UIView.transition(with: nameLabel, duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.nameLabel.text = human.name
        })
    }
}

class HumanAnnotation: MKPointAnnotation {
    var image: UIImageView!
}
